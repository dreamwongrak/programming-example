
#This code calculates the distance between points given in an input.txt file.

#Import data from input.txt
data = File.readlines 'input.txt'

coordinates = []

#Split each line and put them into an Array called coordinates
data.each do |line| 
coordinates << line.split(" ") # x is (coordinates[i])[0] , y is (coordinates[i])[1]
end

#function to calculate the distance between point_1 and point_2
def distance(point_1,point_2)
	x1 = point_1[0].to_f
	y1 = point_1[1].to_f
	x2 = point_2[0].to_f
	y2 = point_2[1].to_f
	d = Math.sqrt((x2 - x1)**2 + (y2 - y1)**2)
	answer = "%.4f" % d #to give the answer up to 4 digit decimals
	print answer.ljust(20) #.ljust 20 will keep answer's length as 20
end

#Print the table head for the result
puts "Distances Between Two Different Coordinates"
print " ".ljust(20) #keep the top left corner of the table empty
coordinates.each {|point| print "\(#{point[0]} , #{point[1]}\)".ljust(20)} 
#point[0] is x, point[1] is y
puts ""

#Calculate distances between each pair of data and display them in a data table
coordinates.each_index do | i |
	x = (coordinates[i])[0];
	y = (coordinates[i])[1];
		print "\(#{x} , #{y}\)".ljust(20)  
		coordinates.each_index do | j |
		d = distance(coordinates[i],coordinates[j])
		end
	puts ""
end

#Note:

#  	An Alternative to Read Lines
# 	data = File.foreach( 'input.txt' ) do |line|
# 	puts line
# 	end

# 	puts data.class / to check class of data



