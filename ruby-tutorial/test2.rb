
class Animal
    def initialize(name, weight)
        puts "Create Animal => name: #{name}, weight: #{weight}"
        @name = name
        @weight = weight
    end

    def walk
        puts "name: #{@name}, weight: #{@weight} => walk"
    end

    def eat
        puts "name: #{@name}, weight: #{@weight} => eat"
    end

    def set_name(name)
        @name = name
    end

    def get_name
        @name
    end

    def set_weight(weight)
        @weight = weight
    end

    def get_weight
        @weight
    end

    private

    def sleep
        puts 'sleep'
    end

    protected

    def fly
        puts 'fly'
    end
end

class Dog < Animal
    def run
        puts "name: #{@name}, weight: #{@weight} => walk"
        # fly()
    end

    def walk
        super
        puts "name: #{@name}, weight: #{@weight} => dog walk"
    end
end

animal = Animal.new("jame", 10)
animal.walk
animal.eat

puts "=========================="
dog = Dog.new("start", 20)
dog.walk
dog.eat
dog.run