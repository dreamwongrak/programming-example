# class Example
#     def initialize
#     end
# end

# obj = Example.new
# puts obj

class Car
    def run
        puts "Car #{@color}: Run"
        bomb()
    end

    def get_color
        @color
    end

    def set_color=(color)
        @color = color
    end

    private

    def bomb
        puts 'bomb'
    end
end

car = Car.new
car.set_color = 'Red'
puts car.get_color
car.run

# car2 = Car.new
# car2.set_color = 'Blue'
# puts car2.get_color
# car2.run

# # Get color ไว้ดึง @color
# puts car.get_color

# puts car
# puts car2

# car.bomb