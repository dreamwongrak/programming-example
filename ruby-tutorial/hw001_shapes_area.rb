class Shape

	def initialize(name,type_of_shape)
		@name = name
		@shape = type_of_shape
		puts "You have created a new #{type_of_shape} named \" #{name} \""
	end

	def area
		puts "You should create a new circle, triangle or rectangle and use the method \"compute_area\". "
	end

	def set_color(color="transparent")
		@color = color
		puts "The color is #{@color}."
	end

    def get_name
        @name
    end

    def get_type
        @type_of_shape
    end

    def get_color
        @color
    end
end

class Triangle < Shape

	def initialize(base = 0,height = 0)
		@base = base
		@height = height
		puts ""
		puts "Your shape is a triangle with: "
		puts "  Base = #{base} , Height = #{height}"
	end

	def compute_area
		@a = (0.5*@base*@height)
		puts "The area of your triangle is #{@a}"
	end
end

class Rectangle < Shape

	def initialize(width = 0,length = 0)
		@width = width
		@length = length
		puts ""
		puts "Your shape is a rectangle with: "
		puts "  Width = #{width} , Length = #{length}"
	end

	def compute_area
		@area_rectangle = (@width * @length)
		puts "The area of your rectangle is #{@area_rectangle}"
	end
end

class Circle < Shape

	def initialize(radius = 0)
		@radius = radius
		puts ""
		puts "Your shape is a circle with: "
		puts "  Radius = #{radius}"
	end

	def compute_area
		@area_circle = "%.2f" % (Math::PI * @radius ** 2)
		puts "The area of your circle is #{@area_circle}"
	end
end

a = Shape.new("newshape","circle")
a.set_color
a.area

b = Circle.new(21)
b.compute_area
b.set_color("green")
c = Rectangle.new(10,10)
c.compute_area
c.set_color("red")
d = Triangle.new(5,10)
d.compute_area
c.set_color("pink")









