dial_book = {"Pranburi" => "77220",
  "Huahin" => "77100"}
  #Pranburi is a key and 77220 is its value.


  def get_city_names(somehash)
    somehash.keys #return keys of the "somehash"
  end

  def get_area_code(somehash, key)
    somehash[key]
  end

  loop do 
    puts "Do you want to lookup an area code based on a city name? (Y/N)"
    answer = gets.chomp.downcase
    break if answer != "y"
    puts "Which city do you want to lookup the area code for?"
    puts get_city_names(dial_book)
    puts "Enter your selection"
    prompt = gets.chomp
    if dial_book.include?(prompt)
        puts "The area code for #{prompt} is #{get_area_code(dial_book,prompt)}"
    else
        puts "You input an invalid city name."
    end
  end


