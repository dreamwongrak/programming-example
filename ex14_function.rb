
#a function that adds two numbers
def add_numbers(num1, num2)
	return num1 + num2
end

#Show the result of  the add_numbers function
puts add_numbers(5,10)

#This is to demonstrate that any value that you change inside the function
#is not going to affect the value outside of the function
x = 1
def change_value_x(x)
	return x = 5
end

z = change_value_x(x)

puts "The value of x inside the function is " + "#{z}"
puts "The value of x outside of the function is " + "#{x}"

puts add_numbers(x, z)
