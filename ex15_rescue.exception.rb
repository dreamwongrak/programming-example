print "Enter a number, A:"
num1 = gets.to_i
print "Enter another number, B:"
num2 = gets.to_i
puts " A/B "

begin 
	answer = num1/num2

rescue #add this for the case that an error happens when B = 0
	puts " A number can't be divided by zero! Please try again."
	exit
end

puts "#{num1} / #{num2} = #{answer}"