puts "5 <=> 10 = " + (5 <=> 10).to_s
puts "10  <=> 5 = " + (10 <=> 5).to_s
puts "true && false  = " + (true && false).to_s
puts "true || false  = " + (true || false).to_s
puts "!false = " + (!false).to_s

age = 12
puts "age = " + age.to_s
puts (age >=50)? "Old":"Young"
