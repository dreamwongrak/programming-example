 
names2 = %w{ Somchai Somsak Somsri Sommai Sompong }  
puts names2[0] 
puts names2[3]   

first_name = "Nuchalee"
last_name = "Schwert"
middle_name= "Happy"
full_name = first_name + middle_name + last_name
full_name = "#{first_name} #{middle_name} #{last_name}"

puts full_name

puts full_name.include?("Nuch")

puts full_name.size

puts "Vowels : " + full_name.count("aeiou").to_s

puts "Consonants : " + full_name.count("^aeiou").to_s # "#" is NOT

puts full_name.start_with?("Nu")

puts "Index : " + full_name.index("Ha").to_s 

puts "Index : " + full_name.index("Sc").to_s 