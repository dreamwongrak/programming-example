#A block is everything betwen do ... end or 

def repeat(number)
	while number > 0
	yield
	number -=1
	end
end

repeat(5) {puts "This is puts from yield"}

puts "Use a block with an Array"
arr = [1, 2, 3, 4, 5]

arr.each do | element | 
	puts element  
end

arr.each_index do | index |
  	puts "The element at #{index} is #{arr[index]}"
  	puts "The square of #{arr[index]} is #{arr[index]**2}"
  	puts "The cube of #{arr[index]} is #{arr[index]**3}"
end

hash = {a: 1, b: 2, c: 3, d: :d, e: :e, f: :f}

hash = hash.delete_if do |key, value|
  	key == value
end

puts "Understand \"yield\""
def call_block  
  puts 'Start of method'  
  # you can call the block using the yield keyword  
  yield  
  yield  
  puts 'End of method'  
end  
# Code blocks may appear only in the source adjacent to a method call  
call_block {puts 'In the block'}
puts ""
puts "Understand \"yield\""
def call_block  
  yield('hello', "Nanny")  
end  
call_block {|str1, str2| puts str1 + ' ' + str2.to_s} 

def call_name_age
	yield("John",20)
	yield("Anne",35)
end
call_name_age {|name,age| puts "My name is #{name} and I am #{age} years old"}