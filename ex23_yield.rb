#A block is everything betwen do ... end or 

def repeat(number)
	while number > 0
	yield
	number -=1
	end
end

repeat(5) {puts "This is puts from yield"}

puts "Use a block with an Array"
arr = [1, 2, 3, 4, 5]

arr.each do | element | 
	puts element  
end

arr.each_index do | index |
  	puts "The element at #{index} is #{arr[index]}"
  	puts "The square of #{arr[index]} is #{arr[index]**2}"
  	puts "The cube of #{arr[index]} is #{arr[index]**3}"
end
