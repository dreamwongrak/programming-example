Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: 'profiles#index'

  get '/blog', to: 'profiles#blog'
  get '/about', to: 'about#index'
  get '/web', to: 'profiles#web'
end
