
names1 = [ 'Somchai', 'Somsak', 'Somsri', 'Sommai', 'Sompong' ]  
puts names1[0] # Somchai  
puts names1[3] # Sommai
# this is the same:  
names2 = %w{ Somchai Somsak Somsri Sommai Sompong }  
puts names2[0] 
puts names2[3]   