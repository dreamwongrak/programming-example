#!/home/swiftlet/.rvm/rubies/ruby-2.5.3/bin/ruby
# A method returns the value of the last line  
# Methods that act as queries are often named with a trailing ?  
# Methods that are "dangerous," or modify the receiver, might be named  
# with a trailing ! (Bang methods)  
# A simple method  
def hello  
  'Hello'  
end  
#use the method  
puts hello  
  
# Method with an argument - 1  
def hello1(name)  
  'Hello ' + name  
end  
puts(hello1('Nuchalee'))  
  
# Method with an argument - 2  
def hello2 name2  
  'Hello ' + name2  
end  
puts(hello2 'Nuchy')  

# interpolation refers to the process of inserting the result of an  
# expression into a string literal  
# the interpolation operator #{...} gets calculated separately  
def mtd(arg1="Sring1", arg2="Sring2", arg3="String3")  
  "#{arg1}, #{arg2}, #{arg3}."  
end  
puts mtd  
#Ruby lets you use the default values if you do not assign the value explicitely
puts mtd("ruby")
puts mtd("a","b","c")

# alias new_name old_name  
# When a method is aliased, the new name refers  
# to a copy of the original method's body  

def downer(string)  
  string.downcase  
end  

a = "HELLO"  
downer(a)      # -> "hello"  
puts a         # -> "HELLO"  

# "!" at the end of a method (Bang Method) will replace the new value to the original object. 
def downer(string)  
  string.downcase!  
end  
a = "HELLO"  
downer(a)      # -> "hello"  
puts a         # -> "hello"