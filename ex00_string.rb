=begin  
  Ruby Strings  
  In Ruby, strings are mutable  
=end  
  
puts "Hello World"  
# Can use " or ' for Strings, but ' is more efficient  
puts 'Hello World'  
# String concatenation  
puts 'I like' + ' Ruby'  
# Escape sequence  
puts 'It\'s my Ruby'  
puts "\" Hello! \""

# New here, displays the string three times  
puts 'Hello' * 3  

# Defining a constant  
# More on Constants later, here  
# /satishtalim/ruby_names.html  
PI = 3.1416  
puts PI 
#Use the back-tick or Grave accent ` to parse the command on Linux or Windows
puts `ls`
puts `echo Hello`
#Append to a string
a = "abcd"
puts "a = " + a
puts "Append \" e \" to " + a
puts a << "e"